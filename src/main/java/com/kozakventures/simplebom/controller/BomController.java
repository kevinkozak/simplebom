package com.kozakventures.simplebom.controller;

import java.net.URI;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.kozakventures.simplebom.model.Bom;
import com.kozakventures.simplebom.model.BomComponent;
import com.kozakventures.simplebom.model.template.BomTemplate;
import com.kozakventures.simplebom.service.BomService;
import com.kozakventures.simplebom.service.PartService;

@RestController
public class BomController {

  final Log logger = LogFactory.getLog(getClass());

  @Autowired
  BomService bomService;

  @Autowired
  PartService partService;

  @RequestMapping(value = "/parts", method = RequestMethod.GET)
  public ResponseEntity<List<BomComponent>> getParts() {
    logger.info("GET /parts");
    List<BomComponent> parts = partService.getParts();
    return new ResponseEntity<List<BomComponent>>(parts, HttpStatus.OK);
  }

  @RequestMapping(value = "/boms", method = RequestMethod.GET)
  public ResponseEntity<BomTemplate[]> getBoms() {
    logger.info("GET /boms");
    List<Bom> list = bomService.findAllBoms();
    BomTemplate[] bomTemplate = bomService.convertToTemplate(list);
    return new ResponseEntity<BomTemplate[]>(bomTemplate, HttpStatus.OK);
  }

  @RequestMapping(value = "/boms/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BomTemplate> getBom(@PathVariable("id") int id) {
    logger.info("GET /boms/" + id);
    Bom bom = bomService.findBomById(id);
    if (bom == null) {
      return new ResponseEntity<BomTemplate>(HttpStatus.NOT_FOUND);
    }
    BomTemplate bt = bomService.convertToTemplate(bom);
    return new ResponseEntity<BomTemplate>(bt, HttpStatus.OK);
  }

  @RequestMapping(value = "/boms", method = RequestMethod.POST)
  public ResponseEntity<Void> createNewBom(
      @RequestBody BomTemplate bomTemplate, UriComponentsBuilder ucBuilder) {
    logger.info("POST /boms");
    Bom bom = bomService.convertFromTemplate(bomTemplate);
    bomService.saveBom(bom);
    HttpHeaders headers = new HttpHeaders();
    URI uri = ucBuilder.path("/boms/{id}").buildAndExpand(bom.getId()).toUri();
    headers.setLocation(uri);
    return new ResponseEntity<Void>(headers, HttpStatus.OK);
  }

  @RequestMapping(value = "/boms/{id}", method = RequestMethod.PUT)
  public ResponseEntity<Void> updateBom(@PathVariable("id") int id,
      @RequestBody BomTemplate bomTemplate) {
    logger.info("PUT /boms/" + id);
    Bom currentBom = bomService.findBomById(id);

    if (currentBom == null) {
      return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    Bom newBom = bomService.convertFromTemplate(bomTemplate);
    bomService.updateBom(newBom);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }

  @RequestMapping(value = "/boms/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Bom> deleteBom(@PathVariable("id") int id) {
    logger.info("DELETE /boms/" + id);
    Bom bom = bomService.findBomById(id);
    if (bom == null) {
      return new ResponseEntity<Bom>(HttpStatus.NOT_FOUND);
    }

    bomService.deleteBomById(id);
    return new ResponseEntity<Bom>(HttpStatus.NO_CONTENT);
  }

}
