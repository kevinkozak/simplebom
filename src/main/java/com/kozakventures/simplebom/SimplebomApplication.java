package com.kozakventures.simplebom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplebomApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimplebomApplication.class, args);
	}
}
