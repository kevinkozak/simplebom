package com.kozakventures.simplebom.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kozakventures.simplebom.model.Bom;
import com.kozakventures.simplebom.model.BomComponent;
import com.kozakventures.simplebom.model.template.BomTemplate;
import com.kozakventures.simplebom.model.template.ComponentTemplate;

@Service("bomService")
@Transactional
public class BomServiceImpl implements BomService {

  final Log logger = LogFactory.getLog(getClass());
  private static final AtomicInteger count = new AtomicInteger(0);
  private static final List<Bom> boms = new ArrayList<Bom>();

  PartService partService = new PartServiceImpl();

  {
    // For debug and evaluation purposes, add some sample data
    Bom bom = new Bom();
    bom.setId(1);
    bom.setBomNumber("BOM29");
    bom.setDescription("BOM for server build 29");
    PartService ps = new PartServiceImpl();
    BomComponent c1 = ps.getByPartNumber("CF14JT1K00TR-ND");
    BomComponent c2 = ps.getByPartNumber("CF14JT1K00CT-ND");
    BomComponent c3 = ps.getByPartNumber("S85KCATR-ND");

    bom.setComponentQuantity(c1, 12);
    bom.setComponentQuantity(c2, 1);
    bom.setComponentQuantity(c3, 2);
    saveBom(bom);

    Bom bom2 = new Bom();
    bom2.setId(2);
    bom2.setBomNumber("bom23432");
    bom2.setDescription("BOM for server build 23432");
    bom2.setComponentQuantity(c1, 12);
    bom2.setComponentQuantity(c2, 1);
    bom2.setComponentQuantity(c3, 2);
    saveBom(bom2);
  }

  @Override
  public Bom findBomById(int id) {
    for (Bom bom : boms) {
      if (bom.getId() == id) {
        return bom;
      }
    }
    return null;
  }

  @Override
  public void saveBom(Bom bom) {
    bom.setId(count.getAndIncrement());
    boms.add(bom);
  }

  @Override
  public void updateBom(Bom bom) {
    int index = -1;
    for (Bom bm : boms) {
      index++;
      if (bm.getId() == bom.getId()) {
        break;
      }
    }
    boms.set(index, bom);

  }

  @Override
  public void deleteBomById(int id) {
    Bom bom = findBomById(id);
    if (bom != null) {
      boms.remove(bom);
    }
  }

  @Override
  public List<Bom> findAllBoms() {
    return boms;
  }

  @Override
  public void deleteAllBoms() {
    boms.clear();
  }

  @Override
  public boolean isBomExist(Bom bom) {
    return findBomById(bom.getId()) != null;
  }

  @Override
  public BomTemplate[] convertToTemplate(List<Bom> boms) {
    BomTemplate[] templates = new BomTemplate[boms.size()];
    int i = 0;
    for (Bom bom : boms) {
      BomTemplate bomTemplate = convertToTemplate(bom);
      templates[i++] = bomTemplate;
    }
    return templates;
  }

  private ComponentTemplate[] convertToTemplates(
      Map<BomComponent, Integer> components) {
    ComponentTemplate[] cts = new ComponentTemplate[components.size()];
    int i = 0;
    for (BomComponent c : components.keySet()) {
      ComponentTemplate ct = convertToTemplate(c, components.get(c));
      cts[i++] = ct;
    }
    return cts;
  }

  private ComponentTemplate convertToTemplate(BomComponent component,
      Integer quantity) {
    ComponentTemplate ct = new ComponentTemplate();
    ct.type = component.getType();
    ct.description = component.getDescription();
    ct.number = component.getCorporatePartNumber();
    ct.componentCount = quantity;
    return ct;
  }

  public List<Bom> convertFromTemplate(BomTemplate[] bomTemplates) {
    List<Bom> boms = new ArrayList<>();
    for (BomTemplate bt : bomTemplates) {
      Bom bom = convertFromTemplate(bt);
      boms.add(bom);
    }
    return boms;
  }

  @Override
  public Bom convertFromTemplate(BomTemplate bomTemplate) {
    Bom bom = new Bom();
    bom.setId(bomTemplate.id);
    bom.setBomNumber(bomTemplate.number);
    bom.setDescription(bomTemplate.description);
    Map<BomComponent, Integer> components = convertFromTemplate(bomTemplate.components);
    bom.setComponents(components);
    return bom;
  }

  private Map<BomComponent, Integer> convertFromTemplate(ComponentTemplate[] cts) {
    Map<BomComponent, Integer> components = new LinkedHashMap<>();
    if(cts == null){
      return components;
    }
    for (ComponentTemplate ct : cts) {
      BomComponent bc = partService.getByPartNumber(ct.number);
      if (bc == null) {
        logger.warn(ct.number + " part number could not be found");
        continue;
      }
      int count = ct.componentCount;
      components.put(bc, count);
    }
    return components;
  }

  @Override
  public BomTemplate convertToTemplate(Bom bom) {
    BomTemplate bomTemplate = new BomTemplate();
    bomTemplate.id = bom.getId();
    bomTemplate.number = bom.getBomNumber();
    bomTemplate.description = bom.getBomDescription();
    bomTemplate.components = convertToTemplates(bom.getComponents());
    return bomTemplate;
  }

}
