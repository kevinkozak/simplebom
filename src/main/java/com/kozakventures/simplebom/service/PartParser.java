package com.kozakventures.simplebom.service;

import org.apache.commons.csv.CSVRecord;

import com.kozakventures.simplebom.model.BomComponent;

public interface PartParser {

  BomComponent parse(CSVRecord record);

}
