package com.kozakventures.simplebom.service;

import java.util.List;

import com.kozakventures.simplebom.model.BomComponent;

/**
 * Service to get parts from memory (now they are located in-memory, later from
 * a database)
 * */
public interface PartService {

  BomComponent getByPartNumber(String partNumber);

  List<BomComponent> getParts();

}
