package com.kozakventures.simplebom.service;

import java.util.List;

import com.kozakventures.simplebom.model.Bom;
import com.kozakventures.simplebom.model.template.BomTemplate;

/**
 * Service for manipulating BOMs
 * */
public interface BomService {

  Bom findBomById(int id);

  void saveBom(Bom bom);

  void updateBom(Bom bom);

  void deleteBomById(int id);

  List<Bom> findAllBoms();

  void deleteAllBoms();

  boolean isBomExist(Bom bom);

  BomTemplate[] convertToTemplate(List<Bom> boms);
  
  BomTemplate convertToTemplate(Bom bom);

  Bom convertFromTemplate(BomTemplate bomTemplate);

}
