package com.kozakventures.simplebom.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import com.kozakventures.simplebom.model.BomComponent;

/**
 * Import parts from CSV files
 * */
public class PartImportService {
  final Log logger = LogFactory.getLog(getClass());

  public List<BomComponent> importParts() {
    List<BomComponent> components = new ArrayList<>();
    try {
      DefaultResourceLoader resourceLoader = new DefaultResourceLoader();

      // Import Resistors.csv
      Resource resistorsResource = resourceLoader
          .getResource("classpath:Resistors.csv");
      InputStream resCsv = resistorsResource.getInputStream();
      PartParser rParser = new ResistorParser();
      components.addAll(importParts(rParser, resCsv));

      // Import Capacitors.csv
      Resource capacitorsResource = resourceLoader
          .getResource("classpath:Capacitors.csv");
      InputStream capCsv = capacitorsResource.getInputStream();
      PartParser cParser = new CapacitorParser();
      components.addAll(importParts(cParser, capCsv));

      // Import Diodes.csv
      Resource diodesResource = resourceLoader
          .getResource("classpath:Diodes.csv");
      InputStream diCsv = diodesResource.getInputStream();
      PartParser dParser = new DiodeParser();
      components.addAll(importParts(dParser, diCsv));

    } catch (IOException e) {
      logger.error(e);
    }

    return components;

  }

  private List<BomComponent> importParts(PartParser parser, InputStream csvStream) {
    List<BomComponent> components = new ArrayList<>();

    try {
      InputStreamReader isr = new InputStreamReader(csvStream);
      Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(isr);
      int line = -1;
      for (CSVRecord record : records) {
        line++;
        if (line == 0) {
          continue;
        }

        BomComponent component = parser.parse(record);
        if (component != null) {
          components.add(component);
        } else {
          logger.warn("Parse fail at line " + line + parser);
        }

      }
    } catch (IOException e) {
      logger.error(e);
    }

    return components;

  }

}
