package com.kozakventures.simplebom.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kozakventures.simplebom.model.BomComponent;

@Service("partService")
@Transactional
public class PartServiceImpl implements PartService {

  private static List<BomComponent> parts;

  {
    PartImportService importer = new PartImportService();
    parts = importer.importParts();
  }

  @Override
  public BomComponent getByPartNumber(String partNumber) {
    for (BomComponent part : parts) {
      if (part.getCorporatePartNumber().equals(partNumber)) {
        return part;
      }
    }
    return null;
  }

  @Override
  public List<BomComponent> getParts() {
    return parts;
  }

}
