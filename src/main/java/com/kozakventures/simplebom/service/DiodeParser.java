package com.kozakventures.simplebom.service;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.kozakventures.simplebom.model.BomComponent;
import com.kozakventures.simplebom.model.Diode;

/**
 * Parse a CSVRecord and instantiate a new Diode
 * */
public class DiodeParser implements PartParser {
  final Log logger = LogFactory.getLog(getClass());

  @Override
  public BomComponent parse(CSVRecord record) {
    if (!record.isConsistent()) {
      logger.warn("Record is not consistent " + record.toMap());
    }

    if (record.size() == 18) {
      int id = 0;
      Diode d = new Diode(record.get(id++), record.get(id++), record.get(id++),
          record.get(id++), record.get(id++), record.get(id++),
          record.get(id++), record.get(id++), record.get(id++),
          record.get(id++), record.get(id++), record.get(id++),
          record.get(id++), record.get(id++), record.get(id++),
          record.get(id++), record.get(id++), record.get(id++));
      return d;
    }
    return null;
  }

}
