package com.kozakventures.simplebom.model;

public class Diode implements BomComponent {

  final String type = "DIODE";
  final String datasheetUrl;
  final String imageUrl;
  final String corporatePartNumber;
  final String manufacturerPartNumber;
  final String manufacturer;
  final String description;
  final String unitPrice;
  final String packaging;
  final String voltageZenerNominal;
  final String tolerance;
  final String powerMax;
  final String impedanceMax;
  final String currentReverseLeakage;
  final String voltageForwardMax;
  final String operatingTemperature;
  final String mountingType;
  final String packageCase;
  final String supplierDevicePackage;

  public Diode(String datasheetUrl, String imageUrl,
      String corporatePartNumber, String manufacturerPartNumber,
      String manufacturer, String description, String unitPrice,
      String packaging, String voltageZenerNominal, String tolerance,
      String powerMax, String impedanceMax, String currentReverseLeakage,
      String voltageForwardMax, String operatingTemperature,
      String mountingType, String packageCase, String supplierDevicePackage) {

    this.datasheetUrl = datasheetUrl;
    this.imageUrl = imageUrl;
    this.corporatePartNumber = corporatePartNumber;
    this.manufacturerPartNumber = manufacturerPartNumber;
    this.manufacturer = manufacturer;
    this.description = description;
    this.unitPrice = unitPrice;
    this.packaging = packaging;
    this.voltageZenerNominal = voltageZenerNominal;
    this.tolerance = tolerance;
    this.powerMax = powerMax;
    this.impedanceMax = impedanceMax;
    this.currentReverseLeakage = currentReverseLeakage;
    this.voltageForwardMax = voltageForwardMax;
    this.operatingTemperature = operatingTemperature;
    this.mountingType = mountingType;
    this.packageCase = packageCase;
    this.supplierDevicePackage = supplierDevicePackage;

  }

  @Override
  public String getCorporatePartNumber() {
    return this.corporatePartNumber;
  }

  @Override
  public String getType() {
    return this.type;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
