package com.kozakventures.simplebom.model.template;

/**
 * Template for holding Bom and its BomComponent objects as JSON.
 * */
public class BomTemplate {
  public int id;
  public String number;
  public String description;
  public ComponentTemplate[] components;
}
