package com.kozakventures.simplebom.model.template;

/**
 * Template for BomComponent objects as JSON.
 * */
public class ComponentTemplate {
  public String type;
  public String number;
  public String description;
  public int componentCount;
}
