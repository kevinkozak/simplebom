package com.kozakventures.simplebom.model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Object to hold information for a single bill of materials with methods to
 * update the BOM meta data or components
 * */
public class Bom {
  private int id;
  private String bomNumber;
  private String bomDescription;
  private Map<BomComponent, Integer> components = new LinkedHashMap<>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getBomNumber() {
    return bomNumber;
  }

  public void setBomNumber(String bomNumber) {
    this.bomNumber = bomNumber;
  }

  public String getBomDescription() {
    return bomDescription;
  }

  public void setDescription(String bomDescription) {
    this.bomDescription = bomDescription;
  }

  public Map<BomComponent, Integer> getComponents() {
    return components;
  }

  public void setComponents(Map<BomComponent, Integer> components) {
    this.components = components;
  }

  public void setComponentQuantity(BomComponent bomComponent, Integer quantity) {
    components.put(bomComponent, quantity);
  }

}
