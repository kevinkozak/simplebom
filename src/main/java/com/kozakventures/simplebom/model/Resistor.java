package com.kozakventures.simplebom.model;

public class Resistor implements BomComponent {
  final private String type = "RESISTOR";
  final String datasheetUrl;
  final String imageUrl;
  final String corporatePartNumber;
  final String manufacturerPartNumber;
  final String manufacturer;
  final String description;
  final String unitPrice;
  final String packaging;
  final String series;
  final String resistance;
  final String tolerance;
  final String power;
  final String composition;
  final String features;
  final String temperatureCoefficient;
  final String operatingTemperature;
  final String dimension;

  public Resistor(String datasheetUrl, String imageUrl,
      String corporatePartNumber, String manufacturerPartNumber,
      String manufacturer, String description, String unitPrice,
      String packaging, String series, String resistance, String tolerance,
      String power, String composition, String features,
      String temperatureCoefficient, String operatingTemperature,
      String dimension) {

    this.datasheetUrl = datasheetUrl;
    this.imageUrl = imageUrl;
    this.corporatePartNumber = corporatePartNumber;
    this.manufacturerPartNumber = manufacturerPartNumber;
    this.manufacturer = manufacturer;
    this.description = description;
    this.unitPrice = unitPrice;
    this.packaging = packaging;
    this.series = series;
    this.resistance = resistance;
    this.tolerance = tolerance;
    this.power = power;
    this.composition = composition;
    this.features = features;
    this.temperatureCoefficient = temperatureCoefficient;
    this.operatingTemperature = operatingTemperature;
    this.dimension = dimension;
  }

  @Override
  public String getCorporatePartNumber() {
    return corporatePartNumber;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String getDescription() {
    return description;
  }

//  @Override
//  public String toString() {
//    return this.corporatePartNumber;
//  }

}
