package com.kozakventures.simplebom.model;

public class Capacitor implements BomComponent {

  final String type = "CAPACITOR";
  final String datasheetUrl;
  final String imageUrl;
  final String corporatePartNumber;
  final String manufacturerPartNumber;
  final String manufacturer;
  final String description;
  final String unitPrice;
  final String packaging;
  final String series;
  final String capacitance;
  final String tolerance;
  final String voltageRating;
  final String lifetimeAtTemp;
  final String operatingTemperature;
  final String capacitorType;
  final String applications;
  final String rippleCurrent;
  final String impedance;
  final String dimension;
  final String heightSeatedMax;
  final String surfaceMountLandSize;
  final String mountingType;
  final String packageCase;

  public Capacitor(String datasheetUrl, String imageUrl,
      String corporatePartNumber, String manufacturerPartNumber,
      String manufacturer, String description, String unitPrice,
      String packaging, String series, String capacitance, String tolerance,
      String voltageRating, String lifetimeAtTemp, String operatingTemperature,
      String capacitorType, String applications, String rippleCurrent,
      String impedance, String dimension, String heightSeatedMax,
      String surfaceMountLandSize, String mountingType, String packageCase) {

    this.datasheetUrl = datasheetUrl;
    this.imageUrl = imageUrl;
    this.corporatePartNumber = corporatePartNumber;
    this.manufacturerPartNumber = manufacturerPartNumber;
    this.manufacturer = manufacturer;
    this.description = description;
    this.unitPrice = unitPrice;
    this.packaging = packaging;
    this.series = series;
    this.capacitance = capacitance;
    this.tolerance = tolerance;
    this.voltageRating = voltageRating;
    this.lifetimeAtTemp = lifetimeAtTemp;
    this.operatingTemperature = operatingTemperature;
    this.capacitorType = capacitorType;
    this.applications = applications;
    this.rippleCurrent = rippleCurrent;
    this.impedance = impedance;
    this.dimension = dimension;
    this.heightSeatedMax = heightSeatedMax;
    this.surfaceMountLandSize = surfaceMountLandSize;
    this.mountingType = mountingType;
    this.packageCase = packageCase;

  }

  @Override
  public String getCorporatePartNumber() {
    return this.corporatePartNumber;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

}
