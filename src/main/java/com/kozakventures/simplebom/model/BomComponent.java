package com.kozakventures.simplebom.model;

/**
 * Components that can be placed in a BOM
 * */
public interface BomComponent {

  String getCorporatePartNumber();

  String getType();

  String getDescription();

}
