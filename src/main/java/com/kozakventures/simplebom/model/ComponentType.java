package com.kozakventures.simplebom.model;

public enum ComponentType {

  REGISTER("REGISTER"), CAPACITOR("CAPACITOR"), DIODE("DIODE");
  private String id;

  private ComponentType(String name) {
    this.id = name;
  }

  @Override
  public String toString() {
    return this.id;
  }
}
