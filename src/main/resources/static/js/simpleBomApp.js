var bomApp = angular.module('simpleBomApp', [ 'ngRoute', 'bomControllers' ]);

bomApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/boms', {
		templateUrl : 'partials/bom-list.html',
		controller : 'BomListCtrl'
	}).when('/boms/:bomId', {
		templateUrl : 'partials/bom-detail.html',
		controller : 'BomDetailCtrl'
	}).when('/parts/:bomId', {
		templateUrl : 'partials/part-selector.html',
		controller : 'PartSelectorCtrl'
	}).otherwise({
		redirectTo : '/boms'
	});
} ]);