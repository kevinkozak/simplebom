var bomControllers = angular.module('bomControllers', ['ngCookies']);

bomControllers.controller('BomListCtrl', [ '$scope', '$http',
		function($scope, $http) {
			$http.get('/boms').then(function(response) {
				$scope.boms = response.data;
			});

			$scope.createBom = function(newBom) {
				console.log(newBom);
				$http.post('/boms', newBom).then(function(response) {
					$scope.boms.push(newBom);
					$scope.newBom = "";
					console.log("new Bom add success");
				});

			};

		} ]);

bomControllers.controller('BomDetailCtrl', [ '$scope', '$routeParams', '$http', '$window', '$cookies',
		function($scope, $routeParams, $http, $window, $cookies) {
			$scope.bomId = $routeParams.bomId;
			$http.get('/boms/' + $scope.bomId).then(function(response) {
				$scope.bom = response.data;
				
				// if cookie for 'selectedPart' exists, add it to BOM and remove key
				var partToAdd = $cookies.getObject('selectedPart');
				if(partToAdd != null) {
					console.log("try adding part");
					var bomComponent = {};
					bomComponent.componentCount = 1;
					bomComponent.description = partToAdd.description;
					bomComponent.number = partToAdd.corporatePartNumber;
					console.log(bomComponent);
					$scope.bom.components.push(bomComponent);
					$scope.updateBom($scope.bom);
					$cookies.remove('selectedPart');
					console.log("add part success?");
				}
			});

			$scope.updateBom = function(bom) {
				console.log("update BOM called " + bom.id);
				console.log(bom);

				$http.put('/boms/' + $scope.bomId, bom).then(function(response) {
					console.log("update Bom success");
				});
			};

			$scope.deleteBom = function(bom) {
				console.log("delete BOM called " + bom.id);
				$http.delete('/boms/' + $scope.bomId, bom).then(function(response) {
					console.log("delete Bom success");
					$window.location.href = '#/boms';
				});
			};
			
			$scope.addPart = function() {
				$window.location.href = '#/parts/' + $scope.bomId;
			};
			
		} ]);

bomControllers.controller('PartSelectorCtrl', [ '$scope', '$routeParams', '$http', '$cookies', '$window',
		 function($scope, $routeParams, $http, $cookies, $window) {
			$scope.bomId = $routeParams.bomId;
			$http.get('/parts').then(function(response) {
				$scope.parts = response.data;
				console.log(response);
			});
			
			$scope.select = function(part) {
				console.log("part selected");
				console.log(part);
				$cookies.putObject('selectedPart', part);
				$window.location.href = '#/boms/' + $scope.bomId;
			};
		} ]);
