# README #

Simple BOM is a simple bill of materials application for a given parts database

### How do I get set up? ###

Assumes Java 7 and Gradle installed 

* cd <where you want SimpleBOM>
* mkdir simplebom
* cd simplebom
* git clone https://kevinkozak@bitbucket.org/kevinkozak/simplebom.git
* ./gradlew bootRun
* Once Tomcat is running, navigate to http://localhost:8080/
* [See known issues](https://bitbucket.org/kevinkozak/simplebom/issues?status=new&status=open)

[SimpleBOM 1.0.0 is live](http://simplebom-thekozak.boxfuse.io:8080/)